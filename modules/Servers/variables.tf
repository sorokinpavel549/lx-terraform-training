variable "enviroment" {
  default = "dev"
}


variable "region" {
  type    = string
  default = "eu-west-2"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "common_tags" {
  type = map(any)
  default = {
    Owner   = "pavel.sarokin"
    Project = "terraform"
  }

}

variable "public_subnet_ids" {
  type = any
}

variable "aws_security_group" {
  type = any
}
