terraform {
  backend "s3" {
    bucket = "sorokin-test-bucket-for-terraform"
    key    = "dev/training-project/terraform.tfstate"
    region = "eu-west-2"
  }
}