module "vpc-default" {
  source = "git@gitlab.com:sorokinpavel549/lx-terraform-training.git//modules/Network"
}

module "ec2-default" {
  source             = "git@gitlab.com:sorokinpavel549/lx-terraform-training.git//modules/Servers"
  public_subnet_ids  = module.vpc-default.public_subnet_ids[0]
  aws_security_group = [module.vpc-default.aws_security_group]
}

module "vpc-prod" {
  source              = "git@gitlab.com:sorokinpavel549/lx-terraform-training.git//modules/Network"
  allow_ports         = [22, 80]
  env                 = "prod"
  public_subnet_cidrs = "10.0.5.0/24"

}

module "ec2-prod" {
  source             = "git@gitlab.com:sorokinpavel549/lx-terraform-training.git//modules/Servers" #"../modules/Servers" 
  public_subnet_ids  = module.vpc-prod.public_subnet_ids[0]
  aws_security_group = [module.vpc-prod.aws_security_group]
  enviroment         = "prod"
  instance_type      = "t3.nano"
}